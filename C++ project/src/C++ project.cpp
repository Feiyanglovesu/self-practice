//============================================================================
// Name        : C++.cpp
// Author      : Josh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

using namespace std;

int main() {
	cout << "Hello World!!!" << endl; // prints Hello World!!!


		cout << "Size of char : " << sizeof(char) << endl;

		//input and output
		int A;
		cout <<"Please enter A "<< endl;
		cin >> A;
		cout << "This is A: " << A << endl;

        //boolean in C++
		bool B = true; // true is 1, false is 0
		cout << B << endl;

		//swap two int using method
		int a = 2;
		int b = 5;
		cout << "Before swap, a = " << a << ",b = " << b << endl;
		swap(a,b);
		cout << "After swap, a = " << a << ",b = " << b << endl;

		//random number
		int j; //set seeds
		srand( (unsigned) time(NULL) );
		 for(int i = 0; i < 10; i++ )
		   {
		      // generate random number from 1 to 10
		      j= rand() % 10 + 1;
		      cout <<"random number: " << j << endl;
		   }



		 //array
		 int randomNumber[10];
		 for(int i=0;i<10;i++) //no method for array size
		 {
			 j= rand() % 10 + 1;
			 randomNumber[i] = j;
		 }


		 //vector
		 vector<int> vec;
		 for(int i = 0; i < 5; i++){
		      vec.push_back(i);
		   }
		 cout << "size of vector vec: " << vec.size() << endl;

		 //string
		 char greeting[] = "Hello";
		 string str1 = "Hello";
		 string str2 = "C++";
		 string str3;
		 str3 = str1 + str2;
		 cout << str3 << endl;

		 //pointer
		 int var1 = 1;
		 cout << &var1 << endl;

		 int array2[3] = {10,20,30};
		 int *ptr = array2;
		 for(int i=0;i<3;i++){
			 cout << "address of element:" << ptr <<endl;
			 cout << "value of element:" << *ptr <<endl;
			 ptr ++;

		 }

		 //struct--class in java


	return 0;

}


void swap(int &x,int &y){
	int temp = x;
	x = y;
	y = temp;

}
