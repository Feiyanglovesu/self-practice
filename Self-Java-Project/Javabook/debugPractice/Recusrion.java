package debugPractice;
import java.awt.Color;

import sedgewick.StdDraw;

public class Recusrion {

	public static void chooseColor(){
		double num = Math.random();
	    if(num < 0.25){
	    	StdDraw.setPenColor(Color.BLACK);
	    }
	    else if(num > 0.25 && num < 0.5) StdDraw.setPenColor(Color.BLUE);
	    else if(num > 0.5 && num < 0.75) StdDraw.setPenColor(Color.YELLOW);
	    else if(num > 0.75 && num < 1) StdDraw.setPenColor(Color.CYAN);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StdDraw.setCanvasSize(1000, 1000);
		
	    
		StdDraw.setPenColor(Color.BLACK);
		recursive(5,0.2,0.2, 0.8,0.2,0.5,0.72);

	};
	
	public static void drawTriangle(double x1, double y1, double x2, double y2, double x3, double y3){
		chooseColor();
		double[] xcoord = {x1,x2,x3};
		double[] ycoord = {y1,y2,y3};
		StdDraw.polygon(xcoord, ycoord);
	};
	
	public static void recursive(int order,double x1, double y1, double x2, double y2, double x3, double y3){
		if(order > 0){
			recursive(order-1,x1,y1,(x1+x2)/2,y2,(x1+x3)/2,(y1+y3)/2);
			recursive(order-1,(x1+x2)/2,y1,x2,y2,(x2+x3)/2,(y2+y3)/2);
			recursive(order-1,(x1+x3)/2,(y1+y3)/2,(x2+x3)/2,(y2+y3)/2,x3,y3);
		}
		else{
			drawTriangle(x1,y1,x2,y2,x3,y3);
		}
	};

}
