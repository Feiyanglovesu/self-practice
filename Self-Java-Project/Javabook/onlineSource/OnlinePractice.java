package onlineSource;

import java.util.Calendar;
import java.util.Date;

public class OnlinePractice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis( );
		
		//Number & math class
		Integer x = 7;
		byte bx = x.byteValue();
		double dx = x.doubleValue();
		long lx = x.longValue();
		System.out.println("byte value of x:" + bx + "\ndouble value of x:" + dx + "\nlong value of x:"
				+ lx );
		
		
		
		Integer b = Integer.valueOf("444",16);   // transfer string "444" into integer using Hexadecimal
		System.out.println("444 in Hexadecimal��" + b);
		Integer bindecimal = Integer.valueOf("444",10);
		System.out.println(bindecimal);
		
		Integer y = Integer.valueOf(9);
		Double z = Double.valueOf(5);
		int y1 =Integer.parseInt("9");
		double z1 = Double.parseDouble("5");
		
		//character class
		System.out.println( Character.isUpperCase('i'));
		System.out.println( Character.isUpperCase('I'));
		
		//string class
		String Str = new String("www.runoob.com");
        System.out.println( Str.toUpperCase() );
        
        //stringbuffer
        StringBuffer sBuffer = new StringBuffer("The website: ");
        sBuffer.append("www");
        sBuffer.append(".runoob");
        sBuffer.append(".com");
        System.out.println(sBuffer);  
        System.out.println(sBuffer.reverse());
        
        //time
        Date date = new Date();
        System.out.println(date.toString());
        System.out.printf("Year-Month-date��%tF%n",date); //use printf for format
        
        Calendar c1 = Calendar.getInstance();
        c1.set(2009, 6 - 1, 12);

        
        
        long end = System.currentTimeMillis( );
        long totalTime = end - start;
        System.out.println("The total running time(Millissecond):" + totalTime);
	}

}
