package onlineSource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import onlineSource.Box;

public class DataStructure {
	//enumeration
	public enum days{
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday,
		Saturday,
		Sunday
		
	};
	
	//����
	public static <E> void printArray( E[] inputArray )
	   {
	             
	         for ( E element : inputArray ){        
	            System.out.printf( "%s ", element );
	         }
	         System.out.println();
	    };

    public static <E extends Comparable<E>> E minimum(E a,E b, E c){
    	E minimum = a; 
    	if(b.compareTo(minimum) < 0){
    		minimum = b;
    	}
    	
    	if(c.compareTo(minimum) < 0){
    		minimum = c;
    	}
    	
    	return minimum;
    }
    
    //����ͨ���
    public static void getfirst(List<?> data){
    	System.out.println("First element:" + data.get(0));
    }
    
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		days first = days.Sunday;
		
		//vector
		Vector<Double> v = new Vector();
		v.add(10.5);
		System.out.println(v.capacity() + " " + v.size());
		
		Vector<Box> BV = new Vector(7);
		Box box1 = new Box(7,7,7);
		BV.addElement(box1);
		System.out.println(BV.size());
		
		
		//iterator
		List<String> list = new ArrayList<String>();
	    list.add("Hello");
	    list.add("World");
	    list.add("HAHAHAHA");
	    getfirst(list);
	    
	    List<Double> doublelist = new ArrayList<Double>();
	    doublelist.add(5.7);
	    doublelist.add(7.2);
	    doublelist.add(2.3);
	    getfirst(doublelist);
	    
	    Iterator<Double> dite = doublelist.iterator();
	    while(dite.hasNext()){
	    	System.out.println(dite.next());
	    };
	    
	    Iterator<String> ite = list.iterator();
	    
	    while(ite.hasNext())
	    {
	         System.out.print(ite.next() + " ");
	    };
	    
	    //����
	    Integer[] intArray = { 1, 2, 3, 4, 5 };
        Double[] doubleArray = { 1.1, 2.2, 3.3, 4.4 };
        Character[] charArray = { 'H', 'E', 'L', 'L', 'O' };
 
        System.out.println( "\nint array:" );
        printArray( intArray  ); 
 
        System.out.println( "\ndouble array:" );
        printArray( doubleArray );
 
        System.out.println( "\nchar array:" );
        printArray( charArray ); 
        
        double mini = minimum(2.3,4.5,6.8);
        int miniInt = minimum(2,7,8);
        System.out.println(mini + " " + miniInt);

	}

}
