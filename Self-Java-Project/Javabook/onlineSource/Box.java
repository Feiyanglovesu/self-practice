package onlineSource;

public class Box {
	public double height;
	public double length;
	public double width;
	
	public Box(){
		this.height = 5;
		this.length = 5;
		this.width = 5;
	};
	
	public Box(double h,double l, double w){
		this.height = h;
		this.length = l;
		this.width = w;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	};
	
	

}
