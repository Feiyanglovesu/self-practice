package bookPractice;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ScannerPractice {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		File data = new File("Javabook/bookPractice/stepss.csv");
		Scanner input = new Scanner(data);
		
		String splitter = ",";
		ArrayList<Double> xdatas = new ArrayList<Double>();
		ArrayList<Double> ydatas = new ArrayList<Double>();
		ArrayList<Double> zdatas = new ArrayList<Double>();
		
		while(input.hasNext() == true){
			String inputString = input.nextLine();
			String[] splitted = inputString.split(splitter);
			Double x = Double.parseDouble(splitted[0]);
			Double y = Double.parseDouble(splitted[1]);
			Double z = Double.parseDouble(splitted[2]);
			xdatas.add(x);
			ydatas.add(y);
			zdatas.add(z);
			
		}
		input.close();
		
		for(int i=0;i<xdatas.size();i++){
			System.out.print(xdatas.get(i) + " " + ydatas.get(i) + " " + zdatas.get(i));
			System.out.println();
		}

	}

}
