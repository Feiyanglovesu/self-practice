package bookPractice;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter a string");
		
		
		String s = input.nextLine();
		String filtered= filter(s);
		boolean isPalindrome = isPalindrome(s);
		System.out.println("Filtered String: "+filtered);
		System.out.println("Palindrome: " + isPalindrome);

	}
	
	
	public static boolean isPalindrome(String s){
		String s1 = filter(s);
		String s2 = reverseString(s1);
		
		if(s1.equals(s2)){
			return true;
			
		}
		else
		{
			return false;
			
		}
		
	}
	
	
	public static String filter(String s){
		StringBuilder SB = new StringBuilder();
		for(int i=0;i<s.length();i++){
			if(Character.isLetterOrDigit(s.charAt(i))){
				SB.append(s.charAt(i));
			}
		}
		
		return SB.toString();
		
	}
	
	public static String reverseString(String s){
		StringBuilder SB = new StringBuilder(s);
		SB.reverse();
		String reversed = SB.toString();
		return reversed;
		
	}

}
