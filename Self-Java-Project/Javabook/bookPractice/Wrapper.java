package bookPractice;

public class Wrapper {
	public static void main(String[] args){
		String a = "I love Yangzhou";
		String b = a.replace('o', 'z');
		String splitter = " ";
		String[] c = a.split(splitter); // split a string using splitter
		
		int[] array = new int[5];
		int[][] twoDarray = new int[3][4];
		
		for(int i=0;i<c.length;i++){
			System.out.println(c[i]); 
			
		}
		 
		//System.out.print(b);
		
		
		String stringNumber = "100.25"; // convert string to double using parseDouble()
		Double e = Double.parseDouble(stringNumber);
		System.out.println(e);
		
		String longString = "I love Yangzhou, I love Yangzhou";
		String longString2 = longString.replaceAll("Yangzhou", "Beijing");
		System.out.println(longString2);
	    System.out.println(longString.matches("I.*"));  //"I.*" ������ʽ
		
	    char[] charArray = longString.toCharArray(); //convert String to char array
	    for(int i=0;i<charArray.length;i++){
	    	System.out.print(charArray[i]);
	    }
	    System.out.println();
	    
	    char[] abc = {'a','b','c'}; //convert char array to string
	    String abcS = String.valueOf(abc);
	    System.out.println(abcS);
	    
	    //use of Stringbuilder
	    StringBuilder stringBuilder = new StringBuilder();
	    stringBuilder.append("Welcome");
	    stringBuilder.append(" to ");
	    stringBuilder.append("Java");
	    String markString = "Java";
	    int start = stringBuilder.indexOf(markString);
	    int end = start + markString.length();
	    System.out.println(stringBuilder + " Java is at " + start + " to " + end);
	     
	    stringBuilder.replace(start, end, "JavaWorkspace");
	    System.out.println(stringBuilder);
	    
	}

}
