package bookPractice;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Stream {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//create an output stream
		FileOutputStream output = new FileOutputStream("temp.txt");
		for(int i=1;i<10;i++){
			output.write(i);
		}
		
		//creat an input stream
		FileInputStream input = new FileInputStream("temp.txt");
		while(input.available()>0){
			System.out.print(input.read() + " ");
		}
		System.out.println();
		input.close();
		
		DataOutputStream DataOutput = new DataOutputStream(new FileOutputStream("temp2.dat"));
		DataOutput.writeUTF("Josh");
		DataOutput.writeDouble(3.8);
		DataOutput.writeUTF("Lexie");
		DataOutput.writeDouble(3.9);
		
		DataInputStream DataInput = new DataInputStream(new FileInputStream("temp2.dat"));
		while(DataInput.available() > 0){
			System.out.print(DataInput.readUTF() + " ");
			System.out.println(DataInput.readDouble());
		}
		DataInput.close();
		
		//buffered stream: used to improve speed(inherit all methods from inputstream/outputstrem, without adding any)
		DataOutputStream DataOutput2 = new DataOutputStream(
				new BufferedOutputStream(new FileOutputStream("temp2.dat")));
		//reading data from file to a buffered zone, reducing reading times. 
		
		
		//Example: copy file
		

	}

}
