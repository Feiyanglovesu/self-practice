//============================================================================
// Name        : C++.cpp
// Author      : Josh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
#include <string.h>

using namespace std;

class human
{
	public:
		bool Married;
		string name;
		string gender;
		double height;
		double weight;

		human(void){
			this->Married = false;
			this->gender = "male";
			this->name = "Tom";
			this->height = 160;
			this->weight = 100;
		};

		void setHeight(double h){
			this->height = h;
		};

		double getHeight(void){
			return this->height;
		};

		void setWeight(double w){
			this->weight = w;
		};

		double getWeight(void){
			return this->weight;
		};



};

class Box
{
   public:
      double length;
      double breadth;
      double height;

      double getVolume(void)
	{
    	  return length * breadth * height;
	};

      //constructor
      Box(void){
    	this->length = 10;
    	this->breadth = 10;
    	this->height = 10;
      };

      //constructor with parameter
      Box(double h, double b, double l){
    	this->breadth =  b;
    	this->height = h;
    	this->length = l;
      };

      void printBox(void){
    	  cout << "Length: " << this->length << endl;
    	  cout << "breadth: " << this->breadth << endl;
    	  cout << "height: " << this->height << endl;
      };

      //being executed when object is deleted
      ~Box(void){
    	  cout << "Object is being deleted" << endl;
      }
};

//struct practice
struct Student
{
	char name[30];
	double height;
	double weight;
};

void printStudent(struct Student student){
	cout << "Student's name: " << student.name << endl;
	cout << "Student's weight:" << student.weight << endl;
	cout << "Student's height:" << student.height << endl;
	cout << endl;

}

int main() {
	cout << "Hello World!!!" << endl; // prints Hello World!!!

	//Box class practice
	Box mybox;
	mybox.breadth = 9;
	mybox.height = 8;
	mybox.length = 7;
	double volume = mybox.getVolume();
	cout << "The volume of mybox is: " << volume << endl;

	//constructor without parameter
	Box mybox2;
	cout << "box2's height:" << mybox2.height << endl;
	mybox2.printBox();

	//constructor with parameter
	Box mybox3(15,14,13);
	cout << "box3's breadth:" << mybox3.breadth << endl;




	Student s;
	Student t;
	strcpy( s.name,"Josh");
	s.height = 170.5;
	s.weight = 120.5;

	strcpy(t.name,"Lexie");
	t.height = 160.2;
	t.weight = 100.3;
	printStudent(s);
	printStudent(t);


	int a = 100;
	int *pointer; //define it as a pointer using *
	pointer = &a; // the address of a

	cout << "address:" << pointer << endl;
	cout << "value:" << a << endl;

	 int array[3] = {10,20,30};
	 int *ptr = array;
	 for(int i=0;i<3;i++){
		 cout << "address of element:" << ptr <<endl;
		 cout << "value of element:" << *ptr <<endl;
		 ptr ++;

	 }
	 cout << endl;

	 //null pointer
	 int *nptr = NULL;
	 cout << nptr << endl;

	return 0;
}
