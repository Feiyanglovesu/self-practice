/*
 ============================================================================
 Name        : demo5.c

 Author      : Josh
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	//puts("Hello Yangzhou!!!"); /* prints Hello World!!! */
	//return EXIT_SUCCESS;

	printf("Hello world!\n");
	//printf("\n");

	int sum=0;
	for(int i=0;i<5;i++){
		sum = sum + i;
	}

	int sum2 = 0;
	while(sum2<10){
		sum2 ++;
	}

	int arrary[10];
	for(int i=0;i<10;i++){
		arrary[i] = i*2+5;
		printf("%d\n",arrary[i]);

	}

//	printf("The sum value:sum = %d\n", sum);
//	printf("The sum2 value:sum2 = %d\n", sum2);
//	//printf("%d ",sum);
//	//printf("\n");
//	//printf("Sum2 is ");
//	//printf("%d",sum2);
	return 0;
}
